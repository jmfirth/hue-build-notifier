import HueBuildNotifier from "./hue-build-notifier"
import xmpp from "node-xmpp-client";
import fs from "fs";

var str = fs.readFileSync('./config.json');
var config = JSON.parse(str);

var hbn = new HueBuildNotifier(config);
hbn.start();
