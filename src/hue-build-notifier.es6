import hue from "node-hue-api";
import xmpp from "node-xmpp-client";

let HueApi = hue.HueApi;
let lightState = hue.lightState;

/**
 * HueBuildNotifier
 */
export default class HueBuildNotifier {
	constructor(config) {
		this.config = config;
		if (!Array.isArray(this.config.hue.lights))
			this.config.hue.lights = [ this.config.hue.lights ];
		this.config.hue.ip = null;
		this.api = null;
	}

	/**
	 * Start the build notifier
	 * @return {Promise} Returns a promise.
	 */
	start() {
		let self = this;
		return this
			.getIp()
			.then(function() { self.listen(); });
	}

	/**
	 * Gets the IP address of the local base station and stores it for use.
	 * @return {Promise} Returns a promise.
	 */
	getIp() {
		let self = this;
		return hue
			.nupnpSearch()
			.then(function(data) {
				self.config.hue.ip = data[0].ipaddress;
				self.api = new HueApi(self.config.hue.ip, self.config.hue.username);
			});
	}

	/**
	 * Listens for messages via XMPP
	 * @return {[type]} [description]
	 */
	listen() {
		let self = this;
		let jabber = new xmpp.Client({
			jid: this.config.xmpp.jid,
			password: this.config.xmpp.password,
			host: this.config.xmpp.host,
			port: this.config.xmpp.port
		});
		jabber.on('online', function() {
			console.log('>> Listening');
			jabber.send('<presence/>');
		});
		jabber.on('stanza', function(stanza) {
			if (stanza.is('message') && stanza.attrs.type != 'error') {
				let body = stanza.getChildText('body');
				if (!body) return;
				console.log('>> Received:', body);
				self.processMessage(body);
			}
		})
	}

	/**
	 * Process the received message.
	 * @param  {string} message The received message.
	 */
	processMessage(message) {
		let self = this;
		message = message.toLowerCase();
		this.config.hue.states.forEach(function(state, index) {
			if (message.indexOf(state.match) != -1)
				self.setLightColor(self.config.hue.lights, state);
		});
	}

	/**
	 * Sets the lights to the start color.
	 * @param {Array<int>} light The Hue light numbers.
	 */
	setLightsStart(lights) {
		let state = lightState
			.create()
			.on()
			.rgb(this.config.hue.start.red, this.config.hue.start.green, this.config.hue.start.blue);
		this.setLightsState(lights, state);
	}

	/**
	 * Sets the lights to the success color.
	 * @param {Array<int>} light The Hue light numbers.
	 */
	setLightsSuccess(lights) {
		let state = lightState
			.create()
			.on()
			.rgb(this.config.hue.success.red, this.config.hue.success.green, this.config.hue.success.blue);
		this.setLightsState(lights, state);
	}

	/**
	 * Sets the lights to the error color.
	 * @param {Array<int>} lights The Hue light numbers.
	 */
	setLightsError(lights) {
		var state = lightState
		.create()
		.on()
		.rgb(this.config.hue.error.red, this.config.hue.error.green, this.config.hue.error.blue);
		this.setLightsState(lights, state);
	}

	/**
	 * Sets the lights to a color.
	 * @param {Array<int>} lights The lights to change.
	 * @param {Color} color       The target color of the lights.
	 */
	setLightState(lights, color) {
		let self = this;
		let state = lightState
			.create()
			.on()
			.rgb(color.red, color.green, color.blue);
		lights.forEach(function(light) {
			self.api.setLightState(light, state);
		});
	}

	/**
	 * Log a message to the console.
	 * @param  {string} type    The message type.
	 * @param  {string} message The message to output.
	 */
	log(type, message) {
		console.log('>> ' + type + ': ' + message);
	}
}
