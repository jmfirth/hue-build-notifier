# Hue Build Notifier

Listens for build messages over XMPP, changing a Hue light to a custom color.

## Installation

```
npm install -g babel
npm install
```

## Running

```
npm start
```
